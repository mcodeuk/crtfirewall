**CRTFirewall**

The CRTFirewall is a simple tool to read SecureCRT configuration sessions and enable easy modification of the PROXY values.

The Proxies and Sessions must already be defined but then the tool allows single or multiple sessions to be modified to point at a new PROXY

This Project can be cloned but is currently closed to contribution.