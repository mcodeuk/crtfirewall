﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CRTProxyForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CRTProxyForm))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ListBoxSessions = New System.Windows.Forms.ListBox()
        Me.ButtonSelective = New System.Windows.Forms.Button()
        Me.TextBoxFilter = New System.Windows.Forms.TextBox()
        Me.ListBoxProxies = New System.Windows.Forms.ListBox()
        Me.Status = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.LabelAutoProxy = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ButtonRoot = New System.Windows.Forms.Button()
        Me.ButtonAutoProxy = New System.Windows.Forms.Button()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.ListBoxSessions)
        Me.Panel1.Location = New System.Drawing.Point(13, 30)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(492, 380)
        Me.Panel1.TabIndex = 1
        '
        'ListBoxSessions
        '
        Me.ListBoxSessions.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListBoxSessions.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBoxSessions.FormattingEnabled = True
        Me.ListBoxSessions.ItemHeight = 16
        Me.ListBoxSessions.Location = New System.Drawing.Point(0, 0)
        Me.ListBoxSessions.Name = "ListBoxSessions"
        Me.ListBoxSessions.Size = New System.Drawing.Size(492, 380)
        Me.ListBoxSessions.TabIndex = 0
        '
        'ButtonSelective
        '
        Me.ButtonSelective.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButtonSelective.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSelective.Location = New System.Drawing.Point(511, 30)
        Me.ButtonSelective.Name = "ButtonSelective"
        Me.ButtonSelective.Size = New System.Drawing.Size(158, 86)
        Me.ButtonSelective.TabIndex = 2
        Me.ButtonSelective.Text = "Select a Proxy" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "From the List Below"
        Me.ToolTip1.SetToolTip(Me.ButtonSelective, "Click to Update Proxies")
        Me.ButtonSelective.UseVisualStyleBackColor = True
        '
        'TextBoxFilter
        '
        Me.TextBoxFilter.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TextBoxFilter.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxFilter.Location = New System.Drawing.Point(3, 16)
        Me.TextBoxFilter.Name = "TextBoxFilter"
        Me.TextBoxFilter.Size = New System.Drawing.Size(152, 23)
        Me.TextBoxFilter.TabIndex = 5
        Me.ToolTip1.SetToolTip(Me.TextBoxFilter, "Add Text to Filter Sessions By")
        '
        'ListBoxProxies
        '
        Me.ListBoxProxies.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListBoxProxies.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBoxProxies.FormattingEnabled = True
        Me.ListBoxProxies.ItemHeight = 16
        Me.ListBoxProxies.Location = New System.Drawing.Point(514, 122)
        Me.ListBoxProxies.Name = "ListBoxProxies"
        Me.ListBoxProxies.Size = New System.Drawing.Size(155, 212)
        Me.ListBoxProxies.TabIndex = 7
        '
        'Status
        '
        Me.Status.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Status.AutoSize = True
        Me.Status.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Status.Location = New System.Drawing.Point(13, 412)
        Me.Status.Name = "Status"
        Me.Status.Size = New System.Drawing.Size(48, 17)
        Me.Status.TabIndex = 8
        Me.Status.Text = "Status"
        Me.ToolTip1.SetToolTip(Me.Status, "Double Click for Status History")
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.TextBoxFilter)
        Me.GroupBox1.Location = New System.Drawing.Point(511, 369)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(158, 41)
        Me.GroupBox1.TabIndex = 9
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Filter"
        '
        'LabelAutoProxy
        '
        Me.LabelAutoProxy.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelAutoProxy.AutoSize = True
        Me.LabelAutoProxy.Location = New System.Drawing.Point(514, 417)
        Me.LabelAutoProxy.Name = "LabelAutoProxy"
        Me.LabelAutoProxy.Size = New System.Drawing.Size(58, 13)
        Me.LabelAutoProxy.TabIndex = 10
        Me.LabelAutoProxy.Text = "Auto Proxy"
        '
        'ButtonRoot
        '
        Me.ButtonRoot.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButtonRoot.Location = New System.Drawing.Point(514, 340)
        Me.ButtonRoot.Name = "ButtonRoot"
        Me.ButtonRoot.Size = New System.Drawing.Size(70, 23)
        Me.ButtonRoot.TabIndex = 11
        Me.ButtonRoot.Text = "Root"
        Me.ToolTip1.SetToolTip(Me.ButtonRoot, "Change the CRT Root Folder")
        Me.ButtonRoot.UseVisualStyleBackColor = True
        '
        'ButtonAutoProxy
        '
        Me.ButtonAutoProxy.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButtonAutoProxy.Location = New System.Drawing.Point(600, 340)
        Me.ButtonAutoProxy.Name = "ButtonAutoProxy"
        Me.ButtonAutoProxy.Size = New System.Drawing.Size(66, 23)
        Me.ButtonAutoProxy.TabIndex = 12
        Me.ButtonAutoProxy.Text = "AutoProxy"
        Me.ToolTip1.SetToolTip(Me.ButtonAutoProxy, "Update AutoProxy for AutoDetect* Proxies, hold CTRL or Shift to Update URL used")
        Me.ButtonAutoProxy.UseVisualStyleBackColor = True
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(108, 26)
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(107, 22)
        Me.AboutToolStripMenuItem.Text = "About"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 13)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "CRT Sessions"
        '
        'CRTProxyForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(681, 435)
        Me.ContextMenuStrip = Me.ContextMenuStrip1
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ButtonAutoProxy)
        Me.Controls.Add(Me.ButtonRoot)
        Me.Controls.Add(Me.LabelAutoProxy)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Status)
        Me.Controls.Add(Me.ListBoxProxies)
        Me.Controls.Add(Me.ButtonSelective)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "CRTProxyForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "CRT Proxy Settings v"
        Me.Panel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ListBoxSessions As System.Windows.Forms.ListBox
    Friend WithEvents ButtonSelective As System.Windows.Forms.Button
    Friend WithEvents TextBoxFilter As System.Windows.Forms.TextBox
    Friend WithEvents ListBoxProxies As System.Windows.Forms.ListBox
    Friend WithEvents Status As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents LabelAutoProxy As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ButtonRoot As System.Windows.Forms.Button
    Friend WithEvents ButtonAutoProxy As System.Windows.Forms.Button
    Friend WithEvents Label1 As Label
End Class
