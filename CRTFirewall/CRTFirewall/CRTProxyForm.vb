﻿Imports System.IO
Imports System.Net
Imports Microsoft.Win32

Public Class CRTProxyForm
    Private Property autoproxyhost As String
    Private Property autoproxyport As String
    Private Property title As String = ""
    Private Property rootfolder As String = ""
    Private Property proxystring As String = ""
    Private Property history As StatusHistory
    Private Property settings As CRTSettings
    Private Class SessionProperties
        Public Property SelectedState As String
        Public Property CurrentProxy As String
    End Class
    Private Class ProxyInfo
        Public Property ProxyType As String
        Public Property ProxyAddress As String
        Public Property ProxyPort As String
        Public Property ProxyPrompt As String
        Public Property ProxyCommand As String
        Public Property ProxyUser As String
        Public Property ProxyPassword As String
    End Class
    Private Property SessionSettings As Dictionary(Of String, SessionProperties)
    Private Property SelectedSessions As List(Of String)
    Private Property ProxySettings As Dictionary(Of String, ProxyInfo)
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        SessionSettings = New Dictionary(Of String, SessionProperties)
        ProxySettings = New Dictionary(Of String, ProxyInfo)
        SelectedSessions = New List(Of String)
        title = Me.Text
        settings = CRTSettings.Load()
        If settings IsNot Nothing Then
            InitForm()
        Else
            Status.Text = "Unable to Open Settings File, Please Close and retry : " & CRTSettings.ExceptionMessage
        End If
        autoproxyhost = Nothing
        autoproxyport = Nothing

    End Sub

    Private Sub AutoDetectProxy(Optional uri As String = Nothing)
        Dim hoststring As String = Nothing
        If uri = Nothing Then
          uri = settings.AutoDetectURL
        End If
        Dim resource As New Uri(uri)
        Dim request As WebRequest = WebRequest.Create(resource)
        hoststring = resource.Host
        Debug.Print(hoststring)
        Dim p As IWebProxy = request.Proxy
        Dim px As Uri = p.GetProxy(resource)
        Debug.Print(px.Host)
        If px.Host = hoststring Then
            autoproxyhost = "0.0.0.0"
            autoproxyport = 0
            proxystring = "Auto Proxy : Direct"
        Else
            autoproxyhost = px.Host
            autoproxyport = px.Port.ToString
            proxystring = String.Format("Auto:[{0};{1}]", autoproxyhost, autoproxyport)
        End If
        SetTitleText()
        LabelAutoProxy.Text = proxystring
        StatusMsg("Auto Proxy Settings updated to : " & proxystring)
    End Sub

    ' This routine is called when the LABEL displaying the present CRT Config folder is double clicked
    ' This allows an update to be made without adding a button!!
    '

    Private Sub ChangeCRTSource(sender As Object, e As EventArgs)
        settings.CRTConfig = GetCRTConfigFolder()
        settings.Save()
        rootfolder = settings.CRTConfig
        SetTitleText()
        If settings.CRTConfig <> "" Then
            LoadSessions(settings.CRTConfig)
            LoadProxies(settings.CRTConfig)
        End If
    End Sub

    '
    ' This routine gets the CRT Config Folder and checks that the Firewalls and Sessions folder exist within it
    '
    Private Function GetCRTConfigFolder()
        Dim crtcfg As FolderBrowserDialog = New FolderBrowserDialog()
        Dim crtroot As String = ""

        If settings.CRTConfig <> "" Then
            crtcfg.SelectedPath = settings.CRTConfig
            crtroot = crtcfg.SelectedPath
        Else
            Dim regkey As RegistryKey
            regkey = Registry.CurrentUser.OpenSubKey("SOFTWARE\VanDyke\SecureCRT")
            If regkey IsNot Nothing Then crtroot = regkey.GetValue("Config Path", "").ToString
            crtcfg.SelectedPath = crtroot
        End If
        crtcfg.Description = "Select a Folder to use as the base for the CRT Config Files - This should have a Firewalls and Sessions Folder within it! : Current :" & crtroot
        crtcfg.RootFolder = Environment.SpecialFolder.MyComputer
        Dim res As DialogResult
        res = crtcfg.ShowDialog
        If res = DialogResult.OK Then
            crtroot = crtcfg.SelectedPath
        End If

        Dim proxies = Path.Combine(crtroot, "Firewalls")
        Dim sessions = Path.Combine(crtroot, "Sessions")
        If Directory.Exists(crtroot) Then
            If Not Directory.Exists(proxies) OrElse Not Directory.Exists(sessions) Then
                crtroot = ""
            End If
        Else
            crtroot = ""
        End If
        Return crtroot
    End Function

    '
    ' This routine gets the sessions from the CRT Config Folder\Sessions
    ' Each sessions is displayed in the ListBoxSessions with te CRT Config root folder removed to simplify display
    '
    ' A dictionary is created during this process also of the current firewall settings for each session to aid in the search processing
    '
    Public Sub LoadSessions(cfgroot As String, Optional subfolder As String = "")
        Dim sessions As String = Nothing
        If subfolder <> "" Then
            sessions = cfgroot
        Else
            ListBoxSessions.Items.Clear()
            sessions = Path.Combine(cfgroot, "Sessions")
            subfolder = sessions
            ListBoxSessions.SelectionMode = SelectionMode.MultiExtended
        End If
        If Directory.Exists(sessions) Then
            Dim di As DirectoryInfo = New DirectoryInfo(sessions)
            Dim fi() As FileInfo = di.GetFiles()
            For Each fie As FileInfo In fi
                If fie.Extension = ".ini" Then
                    Dim fileonly As String = Path.GetFileNameWithoutExtension(fie.FullName)
                    If fileonly <> "__FolderData__" Then
                        If Not SessionSettings.ContainsKey(fie.FullName) Then
                            Dim sessprop As SessionProperties = New SessionProperties
                            sessprop.CurrentProxy = GetCurrentProxy(fie.FullName)
                            sessprop.SelectedState = ""
                            SessionSettings.Add(fie.FullName, sessprop)
                        End If
                        Dim filename = fie.FullName.Substring(subfolder.Length + 1)
                        filename = filename.Remove(filename.Length - fie.Extension.Length)
                        Dim idx As Integer = -1
                        If TextBoxFilter.Text <> "" Then
                            Dim filter As String = TextBoxFilter.Text.ToLower
                            If filename.ToLower.Contains(filter) Then
                                idx = ListBoxSessions.Items.Add(filename)
                            Else
                                If SessionSettings(fie.FullName).CurrentProxy.ToLower.Contains(filter) Then
                                    ListBoxSessions.Items.Add(filename)
                                End If
                            End If
                        Else
                            idx = ListBoxSessions.Items.Add(filename)
                        End If
                    End If
                End If
            Next
            Dim dis() As DirectoryInfo = di.GetDirectories
            For Each di In dis
                LoadSessions(di.FullName, subfolder:=subfolder)
            Next
        End If
    End Sub

    '
    ' This routine loads the Firewall names from the File names in the CRT config folder\Firewalls
    '
    Public Sub LoadProxies(cfgroot As String)
        Dim autodetectiondone As Boolean = False
        Dim proxies As String = Path.Combine(cfgroot, "Firewalls")
        ProxySettings.Clear()
        If ListBoxProxies.Items.Count > 0 Then
            ListBoxProxies.Items.Clear()
        End If
        ListBoxProxies.Items.Add("None")
        If Directory.Exists(cfgroot) AndAlso Directory.Exists(proxies) Then
            Dim di As DirectoryInfo = New DirectoryInfo(proxies)
            Dim fi() As FileInfo = di.GetFiles()
            ListBoxProxies.SelectionMode = SelectionMode.One
            For Each fie As FileInfo In fi
                If fie.Extension = ".ini" Then
                    Dim filename = fie.FullName.Substring(proxies.Length + 1)
                    filename = filename.Remove(filename.Length - fie.Extension.Length)
                    ProxySettings.Add(filename, GetProxyInfo(fie.FullName))
                    ListBoxProxies.Items.Add(filename)
                    If filename.ToLower.StartsWith("autodetect") Then
                        If Not autodetectiondone Then
                            AutoDetectProxy()
                        End If
                        UpdateAutoProxy(fie.FullName)
                    End If
                End If
            Next
        End If
    End Sub

    Public Sub UpdateAutoProxy(filename As String)
        Dim newlines As New List(Of String)
        Dim changed As Boolean = False
        For Each line As String In File.ReadAllLines(filename)
            Dim p() As String = line.Split("=")
            If p.GetUpperBound(0) = 1 Then
                If p(0) = "S:""Firewall Address""" Then
                    If autoproxyhost <> p(1) Then
                        line = p(0) & "=" & autoproxyhost
                        changed = True
                    End If
                ElseIf p(0) = "D:""Firewall Port""" Then
                    Dim proxyhex As String = Hex(autoproxyport).PadLeft(8, "0")
                    If proxyhex <> p(1) Then
                        line = p(0) & "=" & proxyhex
                        changed = True
                    End If
                End If
            End If
            Debug.Print(line)
            newlines.Add(line)
        Next
        If changed Then
            Dim bupfile As String = filename & "~"
            If File.Exists(bupfile) Then
                File.Delete(bupfile)
            End If
            Try
                File.Copy(filename, bupfile)
                File.Delete(filename)
                File.WriteAllLines(filename, newlines.ToArray)
            Catch ex As Exception

            End Try
        End If
    End Sub

    Public Function GetProxyInfo(filename As String)
        Dim pi As New ProxyInfo
        For Each line As String In File.ReadLines(filename)
            Dim p() As String = line.Split("=")
            If p.GetUpperBound(0) > 0 Then
                If line.Contains("S:""Firewall Type""=") Then
                    pi.ProxyType = p(1)
                ElseIf line.Contains("S:""Proxy Prompt""=") Then
                    pi.ProxyPrompt = p(1)
                ElseIf line.Contains("S:""Proxy Command""=") Then
                    pi.ProxyCommand = p(1)
                ElseIf line.Contains("S:""Firewall User""=") Then
                    pi.ProxyUser = p(1)
                ElseIf line.Contains("S:""Firewall Password""=") Then
                    pi.ProxyPassword = p(1)
                ElseIf line.Contains("D:""Firewall Port""=") Then
                    Dim iport As Integer = Integer.Parse(p(1), Globalization.NumberStyles.HexNumber)
                    pi.ProxyPort = CStr(iport)
                ElseIf line.Contains("S:""Firewall Address""=") Then
                    pi.ProxyAddress = p(1)
                End If
            End If
        Next
        Return pi
    End Function

    '
    ' This is called on Form Close so that the Filter and Formsize can be recorded
    ' The location of the form is remembered by the Data Bindings on the Form!
    '
    Private Sub SaveSettings(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If settings IsNot Nothing Then
            settings.HistoryLocation = history.CloseForm()
            settings.FormSize = Me.Size
            settings.Filter = Me.TextBoxFilter.Text
            settings.MyLocation = Me.Location
            settings.Save()
        End If
    End Sub

    '
    ' This initialises the Form so that the CRT config is remembered and the size and position of the Form
    '
    Private Sub InitForm()
        history = New StatusHistory()
        If Not IsOnScreen(history) Then
            RelocateForm(history)
            settings.HistoryLocation = history.Location
        End If
        history.Location = settings.HistoryLocation
        If settings.FormSize <> New Drawing.Size(0, 0) Then
            Me.Size = settings.FormSize
        End If
        Me.Location = settings.MyLocation
        If Not IsOnScreen(Me) Then
            RelocateForm(Me)
            settings.MyLocation = Me.Location
            settings.Save()
        End If
        If settings.CRTConfig = "" OrElse Not Directory.Exists(settings.CRTConfig) Then
            settings.CRTConfig = GetCRTConfigFolder()
            settings.Save()
        End If
        Me.TextBoxFilter.Text = settings.Filter
        ToolTip1.InitialDelay = 1
        ToolTip1.ToolTipIcon = ToolTipIcon.Info
        ToolTip1.ToolTipTitle = "Selected Proxy Information"

        ToolTip1.SetToolTip(ButtonSelective, "Please Select a Proxy From the List Below")
        rootfolder = settings.CRTConfig
        SetTitleText()

        If settings.CRTConfig <> "" Then
            LoadSessions(settings.CRTConfig)
            LoadProxies(settings.CRTConfig)
        End If

    End Sub



    Public Function IsOnScreen(ByVal form As Form) As Boolean
        Dim screens() As Screen = Screen.AllScreens

        For Each scrn As Screen In screens
            Dim formRectangle As Rectangle = New Rectangle(form.Left, form.Top, form.Width, form.Height)

            If scrn.WorkingArea.Contains(formRectangle) Then
                Return True
            End If
        Next

        Return False
    End Function

    Public Sub RelocateForm(ByVal form As Form)
        Dim wa As Rectangle = Screen.PrimaryScreen.WorkingArea
        form.Location = New Point(Int((wa.Right - form.Size.Width) / 2), Int((wa.Height - form.Size.Height) / 2))
    End Sub


    Private Sub SetTitleText()
        Dim titletext As String = title
        titletext &= Application.ProductVersion.ToString()
        While titletext.EndsWith(".0")
            titletext = titletext.Substring(0, titletext.Length - 2)
        End While
        If rootfolder IsNot Nothing AndAlso rootfolder.Length > 0 Then
            titletext &= " - CRT Root Folder : " & rootfolder
        End If
        If proxystring IsNot Nothing AndAlso proxystring.Length > 0 Then
            titletext &= " - " & proxystring
        End If
        Me.Text = titletext
    End Sub

    Private Sub StatusMsg(fmt As String, ParamArray parms As Object())
        If parms IsNot Nothing AndAlso parms.Length > 0 Then
            Status.Text = String.Format(fmt, parms)
        Else
            Status.Text = fmt
        End If
        history.AddText(Status.Text)
    End Sub

    '
    ' This is called when the CHANGE button is clicked
    ' It called the ChangeFirewall routine for all Sessions that have been selected
    ' 
    Private Sub ButtonSelective_Click(sender As Object, e As EventArgs) Handles ButtonSelective.Click
        Dim proxy As String = "None"
        If ListBoxProxies.SelectedItems.Count > 0 Then
            proxy = ListBoxProxies.SelectedItems.Item(0)
        End If
        If ListBoxSessions.SelectedItems.Count > 0 Then
            Dim errorcount As Integer = 0
            StatusMsg("Updating {0} Entries", ListBoxSessions.SelectedItems.Count)
            For Each lbi As String In ListBoxSessions.SelectedItems
                If ChangeProxy(lbi, proxy) = False Then errorcount += 1
            Next
            If errorcount > 0 Then
                StatusMsg("{0} Entries Changed to Proxy : {1} With {2} Errors", ListBoxSessions.SelectedItems.Count - errorcount, proxy, errorcount)
            Else
                StatusMsg("{0} Entries Changed to Proxy : {1}", ListBoxSessions.SelectedItems.Count, proxy)
            End If

        Else
            MsgBox("I cannot change the Proxy Until You tell me which one to use!", MsgBoxStyle.Exclamation)
        End If
    End Sub

    '
    ' This is where the Firewall is changed within the config
    ' When the config is changed the Dictionary of current firewalls is adjusted in case of errors so it can be changed back
    '
    Private Function ChangeProxy(filename As String, newproxy As String) As Boolean
        Dim root As String = settings.CRTConfig
        Dim fullfilename As String = Path.Combine(root, "Sessions", filename)
        Dim backupfile = fullfilename & ".backup"
        Dim oldfile = fullfilename & ".ini"
        Dim oldproxy As String = SessionSettings(oldfile).CurrentProxy
        If oldproxy <> newproxy Then
            Dim tfile As String = Path.GetTempFileName()
            Dim modified As Boolean = False
            Using ns As StreamWriter = New StreamWriter(tfile)
                For Each line As String In File.ReadAllLines(oldfile)
                    If line.StartsWith("S:""Firewall Name""=") Then
                        line = "S:""Firewall Name""=" & newproxy
                        SessionSettings(oldfile).CurrentProxy = newproxy
                        modified = True
                    End If
                    ns.WriteLine(line)
                Next
                ns.Close()
            End Using
            If modified Then
                File.Copy(oldfile, backupfile, True)
                File.Copy(tfile, oldfile, True)
                File.Delete(tfile)

                'File.Replace(tfile, oldfile, backupfile)
                StatusMsg("Proxy for " & filename & " changed from : " & oldproxy & " to : " & newproxy)
            Else
                File.Delete(tfile)
                StatusMsg("Could not change proxy for " & filename & " to : " & newproxy)
            End If
            Return modified
        Else
            StatusMsg("Proxy Update Not Required for " & filename & " already has Proxy : " & oldproxy)
            Return True
        End If
    End Function

    '
    ' This is called when the Filter Text Box is changed and allows for a simple filter display for the Session List Box ... Simples!!
    '
    Private Sub FilterUpdated(sender As Object, e As EventArgs) Handles TextBoxFilter.TextChanged
        LoadSessions(settings.CRTConfig)
    End Sub

    '
    ' This simple routine reads through the Session file and returns the Name of the Firewall from the config
    '
    Public Function GetCurrentProxy(filename As String) As String
        Dim current As String = "None"
        For Each line As String In File.ReadAllLines(filename)
            If line.StartsWith("S:""Firewall Name""=") Then
                Dim p() As String = line.Split("=")
                If p.GetUpperBound(0) > 0 Then
                    current = p(1)
                    Exit For
                End If
            End If
        Next
        Return current
    End Function

    '
    ' This is called whenever the List Box with sessions has the selected line changed added to or removed
    ' It is called to display the current firewall/proxy in the screen for the most recent selected Session
    '
    Private Sub Clicker(sender As Object, e As EventArgs) Handles ListBoxSessions.SelectedIndexChanged

        Dim ctime As String = Now.ToString
        Dim prefix As String = settings.CRTConfig
        Dim lb As ListBox = CType(sender, ListBox)
        If lb.SelectedItems.Count = 1 Then
            For Each key As String In SelectedSessions
                SessionSettings(key).SelectedState = ""
            Next
            SelectedSessions.Clear()
            Dim itemvalue As String = lb.SelectedItems.Item(0)
            Dim fullname As String = Path.Combine(prefix, "Sessions", itemvalue) & ".ini"
            SessionSettings(fullname).SelectedState = ctime
            SelectedSessions.Add(fullname)
            StatusMsg(itemvalue & " has proxy of " & GetCurrentProxy(fullname))
        Else
            Dim diccount As Integer = SelectedSessions.Count
            For Each itemvalue As String In lb.SelectedItems
                Dim fullname As String = Path.Combine(prefix, "Sessions", itemvalue) & ".ini"
                If SessionSettings(fullname).SelectedState <> "" Then
                    SessionSettings(fullname).SelectedState = ctime
                    diccount -= 1
                Else
                    SelectedSessions.Add(fullname)
                    SessionSettings(fullname).SelectedState = ctime
                    StatusMsg(itemvalue & " has proxy of " & GetCurrentProxy(fullname))
                End If
            Next
            StatusMsg("{0} Sessions Selected", lb.SelectedItems.Count)
            If diccount > 0 Then
                Dim deletelist As New List(Of String)
                For Each key In SelectedSessions
                    If SessionSettings(key).SelectedState <> ctime Then
                        SessionSettings(key).SelectedState = ""
                    End If
                Next
            End If

        End If

    End Sub

    '
    ' This is called whenever the selection is changed on the Firewall list
    '
    Private Sub UpdateButton(sender As Object, e As EventArgs) Handles ListBoxProxies.SelectedIndexChanged
        Dim tipinfo As String = ""
        If ListBoxProxies.SelectedItems.Count > 0 Then
            Dim proxyname As String = ListBoxProxies.SelectedItems.Item(0)
            If proxyname.ToLower = "none" Then
                tipinfo = "No Proxy"
                ButtonSelective.Text = "Change Selected Session(s)" & Environment.NewLine & "To No Proxy"
            Else
                ButtonSelective.Text = "Change Selected Session(s)" & Environment.NewLine & "To Proxy" & Environment.NewLine & proxyname

                Dim pi As ProxyInfo = ProxySettings(proxyname)
                tipinfo = "Proxy : " & proxyname & Environment.NewLine & "Address : " & pi.ProxyAddress & "Port : " & pi.ProxyPort
            End If
            ToolTip1.SetToolTip(ButtonSelective, tipinfo)
        Else
            ButtonSelective.Text = "Select A Proxy 1st"
            tipinfo = "Please select a proxy"
            ToolTip1.SetToolTip(ButtonSelective, tipinfo)

        End If

    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        AboutBox1.ShowDialog()
    End Sub

    Private Sub ButtonRoot_Click(sender As Object, e As EventArgs) Handles ButtonRoot.Click
        settings.CRTConfig = GetCRTConfigFolder()
        settings.Save()
        rootfolder = settings.CRTConfig
        SetTitleText()
        If settings.CRTConfig <> "" Then
            LoadSessions(settings.CRTConfig)
            LoadProxies(settings.CRTConfig)
        End If
    End Sub

    Private Sub ButtonAutoProxy_Click(sender As Object, e As EventArgs) Handles ButtonAutoProxy.Click
        If (ModifierKeys And Keys.Control) OrElse (ModifierKeys And Keys.Shift) Then
            'Debug.Print("Button Click with Control or Shift")
            Dim newURL = InputBox("Enter New AutoDetectURL", "CRTFirewall", settings.AutoDetectURL)
            If newURL IsNot Nothing AndAlso newURL IsNot "" Then
                settings.AutoDetectURL = newURL
            End If
        Else
            'Debug.Print("Button Click")
            AutoDetectProxy()
        End If
    End Sub

    Private Sub ShowStatusHistory(sender As Object, e As EventArgs) Handles Status.DoubleClick
        history.ShowForm()
    End Sub
End Class