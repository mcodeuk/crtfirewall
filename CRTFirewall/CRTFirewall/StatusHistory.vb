﻿Public Class StatusHistory

    Private Property _closing As Boolean = False
    Public Property lastlocation As Drawing.Point
    Public Sub AddText(text As String)
        history.AppendText(Now.ToShortTimeString & " " & text & Environment.NewLine)
    End Sub
    Public Sub ShowForm()
        Me.Visible = True
        Me.Activate()
    End Sub
    Public Function CloseForm() As Drawing.Point
        If Me.Visible Then
            lastlocation = Me.Location
        End If
        _closing = True
        Me.Close()
        Return lastlocation
    End Function

    Private Sub FormIsClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If _closing = False Then
            lastlocation = Me.Location
            e.Cancel = True
            Me.Visible = False
        End If
    End Sub

    Private Sub ButtonClear_Click(sender As Object, e As EventArgs) Handles ButtonClear.Click
        history.Clear()
    End Sub
End Class