﻿Version History

v1.5.1

Added AutoProxyDetect URL options. Default is www.google.com

v1.5

Improved File Handling to use VB Using statements wrapped around files.
Also add Close statements as some of the XML files were left Open causing conflict on rewrite
Changed File.Replace for File.Copy and Delete as File.Replace was not working with NAS files

v1.4

Added logic to read registry looking for SecureCRT session folder
It now also displays from Root Folder of My Computer

v1.3

Modified Auto Proxy display
Added to GIT Source Control - uses gitlab.mcode.uk
Added handler for inability to open settings file

v1.1

Updated to move the buttons for Root and AutoProxy and provide some StatusHistory Information
Moved the settings file to Standard HomeGrown location
Ensured that the Form cannot be Invisible when shifting from Multi Screens to Single Screens

