﻿Imports System.IO
Imports System.Xml.Serialization

Public Class CRTSettings
    Public Property CRTConfig As String
    Public Property FormSize As System.Drawing.Size
    Public Property Filter As String
    Public Property MyLocation As System.Drawing.Point
    Public Property HistoryLocation As System.Drawing.Point
    Public Property AutoDetectURL As String
    Public Shared Property ExceptionMessage As String

    Public Sub New()
        FormSize = New Size(0, 0)
        MyLocation = New Point(0, 0)
        HistoryLocation = New Point(0, 0)
        Filter = ""
        CRTConfig = ""
        AutoDetectURL = "http://www.google.com"
    End Sub

    Public Shared Function GetAppFolder() As String
        Dim appfolder As String = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "HomeGrown\CRTFirewall")
        If Not Directory.Exists(appfolder) Then
            Directory.CreateDirectory(appfolder)
        End If
        Return appfolder
    End Function

    Public Shared Function Load() As CRTSettings
        Dim settings As CRTSettings = New CRTSettings()
        Dim appfile As String = Path.Combine(GetAppFolder, "CRTFirewall.xml")
        Try
            If File.Exists(appfile) Then
                Using reader As New StreamReader(appfile)
                    Dim xml As New XmlSerializer(settings.GetType)
                    settings = xml.Deserialize(reader)
                    reader.Close()
                End Using
            Else
                settings = New CRTSettings
            End If
        Catch ex As Exception
            settings = Nothing
            ExceptionMessage = ex.Message
        End Try

        Return settings
    End Function

    Public Sub Save()
        Dim xml As New XmlSerializer(Me.GetType)
        Dim appfile As String = Path.Combine(GetAppFolder, "CRTFirewall.xml")
        'Dim writer As New StreamWriter(appfile)
        Using writer As New StreamWriter(appfile)
            xml.Serialize(writer, Me)
            writer.Close()
        End Using


    End Sub
End Class
